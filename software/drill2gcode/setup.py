#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

import drill2gcode

setup(
name='drill2gcode',
version=drill2gcode.__version__,
description=drill2gcode.__doc__,
packages=find_packages(),
long_description=open('README.md').read(),
include_package_data=True,
)
