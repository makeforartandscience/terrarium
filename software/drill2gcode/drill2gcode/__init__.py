#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Drill2Gcode's package

author : Thierry Dassé
version : 0.1
date : 04/14/2019
licence : cc-by-nc-sa
"""

__version__ = '0.1'

from .drill2gcode import *


