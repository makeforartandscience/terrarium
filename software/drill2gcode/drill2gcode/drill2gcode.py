#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Drill2Gcode's package

author : Thierry Dassé
version : 0.1
date : 04/14/2019
licence : cc-by-nc-sa
"""

import re

class Drill2Gcode:
    """
    Drill2Gcode provide tools to make a gcode file from excellon file
    This package has been debuged with Kicad 5.0.2 drl files
    """

    def __init__(self,header='header.tap',footer='footer.tap',d_drill_max = 2.0,d_std_mill = 1.0):
        """
        initialise the object
        
        parameters are:
        header : filename of header content
        footer : filename of footer content
        d_drill_max : diameter max you can drill, for higher diameter, the hole will be milled by d_std_mill diameter tool
        d_std_mill : diameter of usable mill to use instead of a drill
        """

        self.holes = {}
        try:
            with open(header) as f:
                self.header = f.read()
        except:
            self.header=''

        try:
            with open(footer) as f:
                self.footer = f.read()
        except:
            self.footer=''

        self.d_drill_max = d_drill_max
        self.d_std_mill = d_std_mill


    def load(self,filename):
        """
        load an excellon file. Support oval hole shape

        parameters are:
        filename : name of excellon file
        """
        
        with open(filename) as f:
            lines = f.readlines()
        
        self.holes = {}
        search_diam_def = '^T([\d]*)C([\d.]*)$'
        diam_def = re.compile(search_diam_def)

        d_current = '';
        search_current ='^T([\d]*)$'
        current = re.compile(search_current)

        search_mill ='^X([-?\d.]*)Y([-?\d.]*)G85X([-?\d.]*)Y([-?\d.]*)$'
        mill = re.compile(search_mill)

        search_drill ='^X([-?\d.]*)Y([-?\d.]*)$'
        drill = re.compile(search_drill)

        for line in lines:
            result = diam_def.search(line)
            if result:
                self.holes[result.group(1)] = {'d':result.group(2),'drill':[],'mill':[]}
            else:
                result = current.search(line)
                if result:
                    d_current = result.group(1)
                else:
                    result = mill.search(line)
                    if result:
                        self.holes[d_current]['mill'].append(list(result.groups()))
                    else:
                        result = drill.search(line)
                        if result:
                            self.holes[d_current]['drill'].append(list(result.groups()))

    def mirrorY(self,x):
        """
        return the PCB board with a Y parallel axis

        parameters are:
        x : x coordinate of the axis
        """

        for key,value in self.holes.items():
            for item in value['mill']:
                item[0] = format(2*x - float(item[0]),".2f")
                item[2] = format(2*x - float(item[2]),".2f")
            for item in value['drill']:
                item[0] = format(2*x - float(item[0]),".2f")

    def makeGcode(self,filename,ext):
        """
        export to gcode files.
        oval shape or diameters higher than d_drill_max are made for milling tools
        all diameters are in different gcode file

        parameters are:
        filename : basename of gcode file
        ext : extension of gcode file

        drilling or milling depth is 1.8mm, travel Z is 1.0mm
        milling is done in 3 passes 0.6mm depth 
        """

        for key,value in self.holes.items():
            if value['mill']:
                with open(filename+'-mill_'+value['d']+ext,mode='w') as f:
                    f.write(self.header)
                    for item in value['mill']:
                        f.write('G00 X'+item[0]+' Y'+item[1]+'\nG01 Z-0.60\n'+
                                'G01 X'+item[2]+' Y'+item[3]+'\nG01 Z-1.20\n'+
                                'G01 X'+item[0]+' Y'+item[1]+'\nG01 Z-1.80\n'+
                                'G01 X'+item[2]+' Y'+item[3]+'\nG01 Z0.00\nG00 Z1.00\n')
                    f.write(self.footer)
            if value['drill']:

                if float(value['d']) <= self.d_drill_max:
                    # make a circular hole with a drill                
                    with open(filename+'-drill_'+value['d']+ext,mode='w') as f:
                        f.write(self.header)
                        for item in value['drill']:
                            f.write('G00 X'+item[0]+'Y'+item[1]+'\nG01 Z-1.80\nG01 Z0.00\nG00 Z1.00\n')
                        f.write(self.footer)
                else:
                    #make a circular hole with a mill (diameter is d_std_mill)
                    dm = self.d_std_mill
                    d = float(value['d'])
                    with open(filename+'-mill_'+format(self.d_std_mill,".2f")+'_'+value['d']+ext,mode='w') as f:
                        f.write(self.header)
                        for item in value['drill']:
                            f.write('G00 X'+format(float(item[0])+(d-dm)/2,".2f")+'Y'+item[1]+'\nG01 Z-0.6\n'+
                                    'G02 X'+format(float(item[0])-(d-dm)/2,".2f")+'Y'+item[1]+'I-'+format((d-dm)/2,".2f")+'J0.00\n'+
                                    'G02 X'+format(float(item[0])+(d-dm)/2,".2f")+'Y'+item[1]+'I'+format((d-dm)/2,".2f")+'J0.00\n'+
                                    'G01 Z-1.2\n'+
                                    'G02 X'+format(float(item[0])-(d-dm)/2,".2f")+'Y'+item[1]+'I-'+format((d-dm)/2,".2f")+'J0.00\n'+
                                    'G02 X'+format(float(item[0])+(d-dm)/2,".2f")+'Y'+item[1]+'I'+format((d-dm)/2,".2f")+'J0.00\n'+
                                    'G01 Z-1.8\n'+
                                    'G02 X'+format(float(item[0])-(d-dm)/2,".2f")+'Y'+item[1]+'I-'+format((d-dm)/2,".2f")+'J0.00\n'+
                                    'G02 X'+format(float(item[0])+(d-dm)/2,".2f")+'Y'+item[1]+'I'+format((d-dm)/2,".2f")+'J0.00\n'+                                    
                                    'G01 Z0.00\nG00 Z1.00\n')
                        f.write(self.footer)
