/*
  phSensor.h - Library for ph sensor, return a value between 0 and 14
  author : Thierry Dassé
  date : 06/20/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#ifndef phSensor_h
#define phSensor_h

#include "Arduino.h"

class phSensor {
  public:
  void init(int sensorPin,float scale,int offset);
  float get();
 
  private:
  int pin,offset;
  float scale;
};

#endif
