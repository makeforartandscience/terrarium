/*
  phSensor.cpp - Library for ph sensor, return a value between 0 and 14
  author : Thierry Dassé
  date : 06/20/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#include "Arduino.h"
#include "phSensor.h"

void phSensor::init(int sensorPin,float scale,int offset) {
	this->pin = sensorPin;
	this->scale = scale;
	this->offset = offset;
}

float phSensor::get() {
	return this->scale*analogRead(this->pin)*5.0/1024+this->offset;
}
