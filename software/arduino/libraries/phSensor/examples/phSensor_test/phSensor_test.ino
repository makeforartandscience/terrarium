
/*
 * Terrarium project
 * 
 *analogSensor library test
 */
 
#include<phSensor.h>

#define phSensorPin A1 

phSensor ph;


void setup() {

  Serial.begin(57600);
  ph.init(phSensorPin,3.5,0); // 3.5 is a good scale value for analogic pHmeter SEN0161 from DFRobot
}

void loop() {

  Serial.println(ph.get());
  delay(300);

}
