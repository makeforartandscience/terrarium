/*
  rgbLed.cpp - Library for RGB LED (on PWM pin)
  author : Thierry Dassé
  date : 05/14/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#include "Arduino.h"
#include "motor.h"

void Motor::init(int motorPin) {
	this->motorPin = motorPin;	
	this->state = false;
	pinMode(motorPin,OUTPUT);
	digitalWrite(motorPin,LOW);
}

void Motor::set(int state) {
	digitalWrite(motorPin,state);
	this->state = state;
}

bool Motor::get() {
	return state;
	
}
