/*
 * Terrarium project
 * 
 * motor test
 */

#include<motor.h>

#define fanPin 2

Motor fan;

void setup() {

  fan.init(fanPin);
}

void loop() {

  fan.set(ON);
  delay(2000);

  fan.set(OFF);
  delay(2000);
}
