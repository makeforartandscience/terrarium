/*
  motor.h - Library for standard DC motor connected on a digital pin and ground
  author : Thierry Dassé
  date : 05/14/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#ifndef motor_h
#define motor_h

#include "Arduino.h"

#define MOTOR_ON 1
#define MOTOR_OFF 0


class Motor {
  public:
  void init(int motorPin);
  void set(int state);
  bool get();
 
  private:
  int motorPin;
  boolean state;
};

#endif
