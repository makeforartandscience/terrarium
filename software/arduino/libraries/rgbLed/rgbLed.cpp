/*
  rgbLed.cpp - Library for RGB LED (on PWM pin)
  author : Thierry Dassé
  date : 03/19/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#include "Arduino.h"
#include "rgbLed.h"

void RGBLed::init(int redPin,int greenPin,int bluePin) {
	this->redPin = redPin;	
	this->greenPin = greenPin;	
	this->bluePin = bluePin;
	pinMode(redPin,OUTPUT);
	pinMode(greenPin,OUTPUT);
	pinMode(bluePin,OUTPUT);
}

void RGBLed::set(int red,int green, int blue) {
	analogWrite(redPin,red);
	analogWrite(greenPin,green);
	analogWrite(bluePin,blue);
	this->red = red;
	this->green = green;
	this->blue = blue;
}

void RGBLed::set(const char* rgbhex) {
	int v;

	if (rgbhex[0] >= 65) {
		v = rgbhex[0] - 55;
	} else {
		v = rgbhex[0] - 48;
	}
	
	if (rgbhex[1] >= 65) {
		v = (v << 4) + rgbhex[1] - 55;
	} else {
		v = (v << 4) + rgbhex[1] - 48;
	}
	analogWrite(redPin,v);
	this->red = v;
	
	if (rgbhex[2] >= 65) {
		v = rgbhex[2] - 55;
	} else {
		v = rgbhex[2] - 48;
	}
	
	if (rgbhex[3] >= 65) {
		v = (v << 4) + rgbhex[3] - 55;
	} else {
		v = (v << 4) + rgbhex[3] - 48;
	}
	analogWrite(greenPin,v);	
	this->green = v;
	
	if (rgbhex[4] >= 65) {
		v = rgbhex[4] - 55;
	} else {
		v = rgbhex[4] - 48;
	}
	
	if (rgbhex[5] >= 65) {
		v = (v << 4) + rgbhex[5] - 55;
	} else {
		v = (v << 4) + rgbhex[5] - 48;
	}
	analogWrite(bluePin,v);	
	this->blue = v;

}

int RGBLed::getRed() {
	return this->red;
}

int RGBLed::getGreen() {
	return this->green;
}

int RGBLed::getBlue() {
	return this->blue;
}

