/*
 * Terrarium project
 * 
 * RGB led test
 */

#include<rgbLed.h>

#define redPin 9
#define greenPin 10
#define bluePin 11 

RGBLed rgb;

void setup() {

  rgb.init(redPin,greenPin,bluePin);

  rgb.set(255,0,255);
  delay(2000);

  rgb.set("0000FF");
  delay(2000);
  
  rgb.set(0,0,0);
}

void loop() {

}
