/*
  rgbLed.h - Library for RGB LED (on PWM pin)
  author : Thierry Dassé
  date : 03/19/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#ifndef rgbled_h
#define rgbled_h

#include "Arduino.h"

class RGBLed {
  public:
  void init(int redPin,int greenPin,int bluePin);
  void set(int red,int green, int blue);
  void set(const char* rgbhex);
  int getRed();
  int getGreen();
  int getBlue();
 
  private:
  int redPin,greenPin,bluePin;
  int red,green,blue;
};

#endif
