/*
 * Terrarium project
 * 
 *analogSensor library test
 */

#include<analogSensor.h>

const int analogSensorPin = A2; 

AnalogSensor light;

void setup() {

  Serial.begin(57600);
  light.init(analogSensorPin,700,40);
  
}

void loop() {

  Serial.println(light.get());
  delay(300);

}
