/*
  analogSensor.h - Library for analog sensor, return value in percent
  author : Thierry Dassé
  date : 05/04/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#ifndef analogSensor_h
#define analogSensor_h

#include "Arduino.h"

class AnalogSensor {
  public:
  void init(int sensorPin,int min,int max);
  int get();
 
  private:
  int pin,min,max;
};

#endif
