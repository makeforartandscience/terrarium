/*
  analogSensor.cpp - Library for analog sensor, return value in percent
  author : Thierry Dassé
  date : 05/04/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#include "Arduino.h"
#include "analogSensor.h"

void AnalogSensor::init(int sensorPin,int min,int max) {
	this->pin = sensorPin;
	this->min = min;
	this->max = max;
	}
	
int AnalogSensor::get() {
	int val = analogRead(this->pin);
	val = map(val,min,max,0,100);
	if (val < 0) {
		return 0;
	} else if (val > 100) {
		return 100;
	} else {
		return val;
	}
}
