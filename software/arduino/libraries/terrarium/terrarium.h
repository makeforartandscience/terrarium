/*
  terrarium.h - general Library for Terrarium Project 
  author : Thierry Dassé
  date : 05/14/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#ifndef terrarium_h
#define terrarium_h

#include "Arduino.h"

boolean startsWith(char *str,char *com);
boolean isAt(char *str,char *com,int n);


#endif
