/*
  terrarium.cpp - general Library for Terrarium Project 
  author : Thierry Dassé
  date : 05/14/2019
  version : 1.0
  license : cc-by-nc-sa 
*/

#include "Arduino.h"
#include "terrarium.h"

boolean startsWith(char *str,char *com) {
	
	int i = 0;
	while (com[i] !='\0') {
		if (str[i] != com[i]) {
			return false;
		}
		i++;
	}
	if (str[i] !=' ') {
		return false;
	}
	return true; 
}

boolean isAt(char *str,char *com,int n) {
	
	int i = 0;
	while (com[i] !='\0') {
		if (str[n+i] != com[i]) {
			return false;
		}
		i++;
	}
	return true; 
}
