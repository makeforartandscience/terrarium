/*
  firmware.ino - program for terrarium lid
  author : Thierry Dassé
  date : 03/19/2019
  version : 1.0.0
  license : cc-by-nc-sa 
*/

#include<terrarium.h>
#include<motor.h>
#include<DHT.h>
#include<rgbLed.h>
#include<analogSensor.h>

#define fanPin 2
#define pumpPin 3
#define DHTPin 4
#define redPin 11
#define greenPin 9
#define bluePin 10 
#define soilMoisturePin A0
#define lightPin A2

char command[32];
char index =0;

Motor fan,pump;
DHT dht(DHTPin,DHT22);
RGBLed rgb;
AnalogSensor light,soilMoisture;

void setup() {

  Serial.begin(57600);
  while (!Serial) {
    ;
  }

  fan.init(fanPin);
  pump.init(pumpPin);
  dht.begin();
  rgb.init(redPin,greenPin,bluePin);
  light.init(lightPin,700,40);
  soilMoisture.init(soilMoisturePin,0,800);
}

void loop() {
  
  if (Serial.available() > 0) {
    if (index == 31) {
      command[index] ='\0';
      index = -1;
    } else {
      char c = Serial.read();
      if (c=='\n') {
        command[index] ='\0';
        index = -1;
      } else {
        command[index++] = c;
      }
    }
  }

  if (index < 0) {
    if (startsWith(command,"GET")) {
      if (isAt(command,"ID",4)) {
        Serial.println("TRARIUM-1.0.0");
      } else if (isAt(command,"TEMP",4)) {
        float t = dht.readTemperature();       
        if (isnan(t)) {
          Serial.println("TEMP: Failed");
        } else {
          Serial.print("TEMP: ");  
          Serial.print(t);
          Serial.println(" °C");
        }
      } else if (isAt(command,"HDTY",4)) {
        float h = dht.readHumidity();       
        if (isnan(h)) {
          Serial.println("HUMIDITY: Failed");
        } else {
          Serial.print("HUMIDITY: ");  
          Serial.print(h);
          Serial.println(" %");
        }
    } else if (isAt(command,"MOIST",4)) {
        Serial.print("SOIL MOISTURE: ");  
        Serial.println(soilMoisture.get());
    } else if (isAt(command,"LIGHT",4)) {
        Serial.print("LIGHT: ");  
        Serial.println(light.get());
    } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"LIGHT")) {
      if (isAt(command,"RED",6)) {
        rgb.set(255,0,0);
      } else if (isAt(command,"BLUE",6)) {
        rgb.set(0,0,255);
      } else if (isAt(command,"GREEN",6)) {
        rgb.set(0,255,0);
      } else if (isAt(command,"WHITE",6)) {
        rgb.set(255,255,255);
      } else if (isAt(command,"MAGENTA",6)) {
        rgb.set(255,0,255);
      } else if (isAt(command,"OFF",6)) {
        rgb.set(0,0,0);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
      
    } else if (startsWith(command,"FAN")) {
      if (isAt(command,"ON",4)) {
        fan.set(ON);
      } else if (isAt(command,"OFF",4)) {
        fan.set(OFF);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"PUMP")) {
      if (isAt(command,"ON",5)) {
        pump.set(ON);
      } else if (isAt(command,"OFF",5)) {
        pump.set(OFF);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else {
      Serial.print("ERROR ");
      Serial.println(command);
    }
    index =0;
  }
}
 
