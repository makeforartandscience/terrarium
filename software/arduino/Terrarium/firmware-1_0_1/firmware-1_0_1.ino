/*
  firmware.ino - program for terrarium lid
  author : Thierry Dassé
  date : 06/20/2019
  version : 1.0.1
  license : cc-by-nc-sa 

  new library ph sensor
  new command GET PH
  
*/

#include<terrarium.h>
#include<motor.h>
#include<DHT.h>
#include<rgbLed.h>
#include<analogSensor.h>
#include<phSensor.h>

#define fanPin 2
#define pumpPin 3
#define DHTPin 4
#define redPin 11
#define greenPin 9
#define bluePin 10 
#define soilMoisturePin A0
#define phPin A1
#define lightPin A2

char command[32];
char index = 0;

Motor fan,pump;
DHT dht(DHTPin,DHT22);
RGBLed rgb;
AnalogSensor light,soilMoisture;
phSensor ph;

void getSensors() {
  Serial.print(soilMoisture.get());
  Serial.print(";");
  Serial.print(dht.readHumidity());
  Serial.print(";");
  Serial.print(dht.readTemperature());
  Serial.print(";");
  Serial.print(light.get());
  Serial.print(";");
  Serial.print(ph.get());
  Serial.print(";");
  Serial.print(pump.get());
  Serial.print(";");
  Serial.print(fan.get());
  Serial.print(";");
  Serial.print(rgb.getRed());
  Serial.print(";");
  Serial.print(rgb.getGreen());
  Serial.print(";");
  Serial.println(rgb.getBlue());
}


void setup() {

  Serial.begin(57600);
  while (!Serial) {
    ;
  }

  fan.init(fanPin);
  pump.init(pumpPin);
  dht.begin();
  rgb.init(redPin,greenPin,bluePin);
  light.init(lightPin,700,40);
  soilMoisture.init(soilMoisturePin,0,800);
  ph.init(phPin,3.5,0);
}

void loop() {
  
  if (Serial.available() > 0) {
    if (index == 31) {
      command[index] ='\0';
      index = -1;
    } else {
      char c = Serial.read();
      if (c=='\n') {
        command[index] ='\0';
        index = -1;
      } else {
        command[index++] = c;
      }
    }
  }

  if (index < 0) {
    if (startsWith(command,"GET")) {
      if (isAt(command,"ID",4)) {
        Serial.println("TRARIUM-1.0.1");
      } else if (isAt(command,"TEMP",4)) {
        float t = dht.readTemperature();       
        if (isnan(t)) {
          Serial.println("TEMP: Failed");
        } else {
          Serial.print("TEMP: ");  
          Serial.print(t);
          Serial.println(" °C");
        }
      } else if (isAt(command,"HDTY",4)) {
        float h = dht.readHumidity();       
        if (isnan(h)) {
          Serial.println("HUMIDITY: Failed");
        } else {
          Serial.print("HUMIDITY: ");  
          Serial.print(h);
          Serial.println(" %");
        }
    } else if (isAt(command,"MOIST",4)) {
        Serial.print("SOIL MOISTURE: ");  
        Serial.println(soilMoisture.get());
     } else if (isAt(command,"PH",4)) {
        Serial.print("WATER PH: ");  
        Serial.println(ph.get());
    } else if (isAt(command,"LIGHT",4)) {
        Serial.print("LIGHT: ");  
        Serial.println(light.get());
    } else if (isAt(command,"RED",4)) {
        Serial.print("RED: ");  
        Serial.println(rgb.getRed());
    } else if (isAt(command,"GREEN",4)) {
        Serial.print("GREEN: ");  
        Serial.println(rgb.getGreen());
    } else if (isAt(command,"BLUE",4)) {
        Serial.print("BLUE: ");  
        Serial.println(rgb.getBlue());
    } else if (isAt(command,"PUMP",4)) {
        Serial.print("PUMP: ");  
        Serial.println(pump.get());
    } else if (isAt(command,"FAN",4)) {
        Serial.print("FAN: ");  
        Serial.println(fan.get());
    } else if (isAt(command,"SENSORS",4)) {
        getSensors();
    } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"LIGHT")) {
      if (isAt(command,"RED",6)) {
        rgb.set(255,0,0);
      } else if (isAt(command,"BLUE",6)) {
        rgb.set(0,0,255);
      } else if (isAt(command,"GREEN",6)) {
        rgb.set(0,255,0);
      } else if (isAt(command,"WHITE",6)) {
        rgb.set(255,255,255);
      } else if (isAt(command,"MAGENTA",6)) {
        rgb.set(255,0,255);
      } else if (isAt(command,"OFF",6)) {
        rgb.set(0,0,0);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
      
    } else if (startsWith(command,"FAN")) {
      if (isAt(command,"ON",4)) {
        fan.set(ON);
      } else if (isAt(command,"OFF",4)) {
        fan.set(OFF);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"PUMP")) {
      if (isAt(command,"ON",5)) {
        pump.set(ON);
      } else if (isAt(command,"OFF",5)) {
        pump.set(OFF);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else {
      Serial.print("ERROR ");
      Serial.println(command);
    }
    index =0;
  }
}
 
