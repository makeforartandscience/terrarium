/*
  firmware.ino - program for terrarium lid
  author : Thierry Dassé, Paul Stoie
  date : 06/22/2019
  version : 1.0.3
  license : cc-by-nc-sa  
*/

#include<terrarium.h>
#include<motor.h>
#include<DHT.h>
#include<rgbLed.h>
#include<analogSensor.h>
#include<phSensor.h>
#include <EEPROM.h>

// sensors and actuators pins of Terrarium shield
#define fanPin 2
#define pumpPin 3
#define DHTPin 4
#define redPin 11
#define greenPin 9
#define bluePin 10 
#define soilMoisturePin A0
#define phPin A1
#define lightPin A2
#define MANUAL 0
#define AUTO 1

char command[32];
char index = 0;

//actuators variables
Motor fan,pump;
RGBLed rgb;

//sensors variables
DHT dht(DHTPin,DHT22);
AnalogSensor light,soilMoisture;
phSensor ph;

/* rules parameters for automode. values are stored in eeprom.
 * You should adjust values after firmware upload using commands below :
 * GET PARAMETERS returns moistMin,moistMax,hdtyMin,hdtyMax,timeMin,timeMax values
 * MOISTMIN value sets moisture min parameter (integer in percent)
 * MOISTMAX value sets moisture max parameter (integer in percent)
 * HDTYMIN value sets humidity Min parameter (integer in percent)
 * HDTYMAX value sets humidity max parameter (integer in percent)
 * Rules are : 
 * if moisture < moistMin then tun pump on
 * if moisture > moistMax then turn pump off
 * if humidity > hdtyMax then tun fan on
 * if humidity < hdtyMin then turn fan off
 * Turn in manual mode to control fan, pump or leds 
*/ 

int moistMin;
int moistMax;
int hdtyMin;
int hdtyMax;
int mode = AUTO;

void getSensors() {
  /* getsensors() function send sensors values and actuators state to the serial bus.
   *  Format is : 
   *  moisture;humidity;temperature;ambient_light;ph;pump;fan;red_light;green_light;blue_light
   */
  Serial.print(soilMoisture.get());
  Serial.print(";");
  Serial.print(dht.readHumidity());
  Serial.print(";");
  Serial.print(dht.readTemperature());
  Serial.print(";");
  Serial.print(light.get());
  Serial.print(";");
  Serial.print(ph.get());
  Serial.print(";");
  Serial.print(pump.get());
  Serial.print(";");
  Serial.print(fan.get());
  Serial.print(";");
  Serial.print(rgb.getRed());
  Serial.print(";");
  Serial.print(rgb.getGreen());
  Serial.print(";");
  Serial.println(rgb.getBlue());
}

void setup() {

  Serial.begin(57600); //set serial communication to 57600 bauds 
  while (!Serial) {
    ;
  }

  // sensors and actuators initialisation
  fan.init(fanPin); 
  pump.init(pumpPin);
  dht.begin();
  rgb.init(redPin,greenPin,bluePin);
  light.init(lightPin,700,40); //photoresistor parameters should be modified to have a 0(darkness) to 100(full light) response
  soilMoisture.init(soilMoisturePin,0,800); //moisture sensor parameters should be modified to have a 0(dry) to 100(water conductivity) response
  ph.init(phPin,3.5,0); //ph sensor scale and offset depends on material 

  //read parameters from eeprom
  moistMin = (int) EEPROM.read(0);
  moistMax = (int) EEPROM.read(1);
  hdtyMin = (int) EEPROM.read(2);
  hdtyMax = (int) EEPROM.read(3);
}

void loop() {
  
  if (Serial.available() > 0) { //read characters on serial bus, end if string length is 31 or \n received 
    if (index == 31) {
      command[index] ='\0';
      index = -1;
    } else {
      char c = Serial.read();
      if (c=='\n') {
        command[index] ='\0';
        index = -1;
      } else {
        command[index++] = c;
      }
    }
  }

  if (index < 0) {
    if (startsWith(command,"GET")) { 
      if (isAt(command,"ID",4)) { // GET ID command
        Serial.println("TRARIUM-1.0.3");
      } else if (isAt(command,"PARAM",4)) { // GET PARAM command
        Serial.print(moistMin);
        Serial.print(";");
        Serial.print(moistMax);
        Serial.print(";");
        Serial.print(hdtyMin);
        Serial.print(";");
        Serial.println(hdtyMax);
      } else if (isAt(command,"TEMP",4)) { // GET TEMP command
        float t = dht.readTemperature();       
        if (isnan(t)) {
          Serial.println("TEMP: Failed");
        } else {
          Serial.print("TEMP: ");  
          Serial.print(t);
          Serial.println(" °C");
        }
      } else if (isAt(command,"HDTY",4)) { // GET HDTY command
        float h = dht.readHumidity();       
        if (isnan(h)) {
          Serial.println("HUMIDITY: Failed");
        } else {
          Serial.print("HUMIDITY: ");  
          Serial.print(h);
          Serial.println(" %");
        }
    } else if (isAt(command,"MOIST",4)) { // GET MOIST command
        Serial.print("SOIL MOISTURE: ");  
        Serial.println(soilMoisture.get());
     } else if (isAt(command,"PH",4)) { // GET PH command
        Serial.print("WATER PH: ");  
        Serial.println(ph.get());
    } else if (isAt(command,"OUTLIGHT",4)) { // GET OUTLIGHT command
        Serial.print("OUTLIGHT: ");  
        Serial.println(light.get());
    } else if (isAt(command,"RED",4)) { // GET RED command
        Serial.print("RED: ");  
        Serial.println(rgb.getRed());
    } else if (isAt(command,"GREEN",4)) { // GET GREEN command
        Serial.print("GREEN: ");  
        Serial.println(rgb.getGreen());
    } else if (isAt(command,"BLUE",4)) { // GET BLUE command
        Serial.print("BLUE: ");  
        Serial.println(rgb.getBlue());
    } else if (isAt(command,"PUMP",4)) { // GET PUMP command
        Serial.print("PUMP: ");  
        Serial.println(pump.get());
    } else if (isAt(command,"FAN",4)) { // GET FAN command
        Serial.print("FAN: ");  
        Serial.println(fan.get());
    } else if (isAt(command,"SENSORS",4)) { // GET SENSORS command
        getSensors();
    } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"LIGHT")) { // LIGHT color command
      if (isAt(command,"RED",6)) {
        rgb.set(255,0,0);
      } else if (isAt(command,"BLUE",6)) {
        rgb.set(0,0,255);
      } else if (isAt(command,"GREEN",6)) {
        rgb.set(0,255,0);
      } else if (isAt(command,"WHITE",6)) {
        rgb.set(255,255,255);
      } else if (isAt(command,"MAGENTA",6)) {
        rgb.set(255,0,255);
      } else if (isAt(command,"OFF",6)) {
        rgb.set(0,0,0);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
      
    } else if (startsWith(command,"FAN")) { // FAN ON|OFF command
      if (isAt(command,"ON",4)) {
        fan.set(MOTOR_ON);
      } else if (isAt(command,"OFF",4)) {
        fan.set(MOTOR_OFF);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"PUMP")) { // PUMP ON|OFF command
      if (isAt(command,"ON",5)) {
        pump.set(MOTOR_ON);
      } else if (isAt(command,"OFF",5)) {
        pump.set(MOTOR_OFF);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"MODE")) { //MODE MANUAL|AUTO command
      if (isAt(command,"AUTO",5)) {
        mode = AUTO;
      } else if (isAt(command,"MANUAL",5)) {
        mode = MANUAL;
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else if (startsWith(command,"MOISTMIN")) { // MOISTMIN integer(0-100) command
      int v = atoi(command+9);
      if (v >0) {
        moistMin = v;
        EEPROM.write(0, (byte)v);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }      
    } else if (startsWith(command,"MOISTMAX")) { // MOISTMAX integer(0-100) command
      int v = atoi(command+9);
      if (v >0) {
        moistMax = v;    
        EEPROM.write(1, (byte)v);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }   
    } else if (startsWith(command,"HDTYMIN")) {  // HDTYMIN integer(0-100) command
      int v = atoi(command+8);
      if (v >0) {
        hdtyMin = v;    
        EEPROM.write(2, (byte)v);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }   
    } else if (startsWith(command,"HDTYMAX")) {  // HDTYMAX integer(0-100) command
      int v = atoi(command+8);
      if (v >0) {
        hdtyMax = v;    
        EEPROM.write(3, (byte)v);
      } else {
        Serial.print("ERROR ");
        Serial.println(command);
      }
    } else {
      Serial.print("ERROR ");
      Serial.println(command);
    }
    index =0;
  }

  if (mode == AUTO) { // auto mode
    if ((soilMoisture.get() < moistMin) and (pump.get() == 0)) {
      pump.set(MOTOR_ON);
      getSensors();
    } else if ((soilMoisture.get() > moistMax) and (pump.get() == 1)) {
      pump.set(MOTOR_OFF);
      getSensors();
    } else if ((dht.readHumidity() < hdtyMin) and (fan.get() == 1)) {
      fan.set(MOTOR_OFF);
      getSensors();
    } else if ((dht.readHumidity() > hdtyMax) and (fan.get() == 0)) {
      fan.set(MOTOR_ON);
      getSensors();
    }
 
  }
}
