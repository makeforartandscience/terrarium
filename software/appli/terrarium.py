#!/usr/bin/env python
# -*- coding: utf-8 -*-

# terrarium.py
#
#  author : Thierry Dassé
#  date : 05/05/2019
#  version : 1.0
#  license : cc-by-nc-sa 
#
# usage : python terrarium.py
#
# connect to terrarium on USB
# prompt >>> and wait commands
#
# available commands :
# ID get firmware id, current is TRARIUM-1.0.0
# EXIT quit the program
#

import serial
import serial.tools.list_ports
import re
import requests
import calendar
import time
import json
import schedule


url = "http://08232ac8.ngrok.io/api/"
terrariumId = "5d0e2603123e83d4a16408b5"

def connect(ports):
    for port in ports:
        try:
            ser = serial.Serial(port, baudrate = 57600,timeout = 1)
            time.sleep(2)
            ser.write(b'GET ID\n')
            ret = ser.readline().decode().split('\r\n')[0]
            if re.match('TRARIUM',ret):
                return (ser,port,ret)
            else:
                ser.close()
        except e:
            print(e)
            pass

def sendData( data ):

    response = requests.post(url + "data", json=data, headers={"Content-Type": "application/json"})

    print(response)
    print(response.text)

    # log on additional data viewer
    requests.post( "https://endpoint1.collection.eu.sumologic.com/receiver/v1/http/ZaVnC4dhaV0x4SSOvc6yNR9kHn8lFLM5qyuXCuGf5SAM7Ojb5VBnFHPV_yNLVwFbJU21SL_3mfj3JLO4RCnsTIf5zY9XszFfXzqvnPiW9bD6iQ1i-iuzmQ==", json=data, headers={"Content-Type": "application/json"} )


def collectAutomationData():

    # automationData = requests.get( url + "/automation/terrarium/" + "1234",  )

    automationData = "{\"fan\": true, \"pump\": false, \"light\": \"GREEN\"}"

    print( json.load( automationData ) )

    return automationData


def getSensorData(command, serial):

    com = command + '\n'
    serial.write(com.encode('ascii'))

    result = serial.readline().decode().split('\r\n')[0]

    return result

ports = [port.device for port in serial.tools.list_ports.comports()]

res = connect(ports)


def parseInt(value):
    # replace after getting sensor data without metric information
    return int(''.join(filter(str.isdigit, value)))


def importJob():

    ser, port, firmware = res

    sensorData = {
        "tid" : terrariumId,
        "timestamp" : calendar.timegm(time.gmtime()),
        "ambientLight" : parseInt( getSensorData("GET LIGHT", ser)),
        "soilHumidity" : parseInt(getSensorData("GET MOIST", ser)),
        "ph": parseInt(getSensorData("GET PH", ser )),
        "airHumidity": parseInt(getSensorData("GET HDTY", ser)),
        "temperature": parseInt(getSensorData("GET TEMP", ser))
    }

    print(sensorData)
    sendData(sensorData)

    # automationData = receiveData()


schedule.every(10).seconds.do(job)

while True:
    schedule.run_pending()
    time.sleep(1)