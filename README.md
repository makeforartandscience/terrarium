#Terrarium Project

Documentation is on the [wiki](https://framagit.org/makeforartandscience/terrarium/wikis/home).

## 2019-06-26 : standard and RTC versions split
- arduino firmware version 1.0.3 and 1.0.3-RTC

## 2019-06-22 : Hackathon in Cluj-Napoca (Romania)
- auto mode rules definition
- arduino firmware version 1.0.3
- Real Time Clock tested with data logger rduino shield (RTC connected on I2C pins A4 & A5)




