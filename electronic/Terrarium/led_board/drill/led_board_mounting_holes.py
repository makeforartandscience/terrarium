#!/usr/bin/python

import drill2gcode

project = drill2gcode.Drill2Gcode(header='../../gcode_header/header.tap', footer='../../gcode_header/footer.tap')
project.load('../gerber/led_board-NPTH.drl')
project.mirrorY(45)
project.makeGcode('../gcode/led_board','.tap')
