EESchema Schematic File Version 4
LIBS:led_board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5CBB17E2
P 6200 2600
F 0 "J1" H 6280 2592 50  0000 L CNN
F 1 "Conn_01x04" H 6280 2501 50  0000 L CNN
F 2 "Components:WR-WTB-H_04" H 6200 2600 50  0001 C CNN
F 3 "~" H 6200 2600 50  0001 C CNN
	1    6200 2600
	1    0    0    -1  
$EndComp
Text Label 5500 2500 2    50   ~ 0
RED
Text Label 6000 2600 2    50   ~ 0
GND
Text Label 5500 2700 2    50   ~ 0
BLUE
Text Label 5500 2800 2    50   ~ 0
GREEN
$Comp
L Device:LED_RABG D5
U 1 1 5CBB1A3F
P 4350 2100
F 0 "D5" H 4350 2597 50  0000 C CNN
F 1 "LED_RABG" H 4350 2506 50  0000 C CNN
F 2 "Components:LED_RGB_D5mm" H 4350 2050 50  0001 C CNN
F 3 "~" H 4350 2050 50  0001 C CNN
	1    4350 2100
	-1   0    0    -1  
$EndComp
Text Label 4550 1900 0    50   ~ 0
RED
Text Label 4550 2100 0    50   ~ 0
GREEN
Text Label 4550 2300 0    50   ~ 0
BLUE
Text Label 4150 2100 2    50   ~ 0
GND
$Comp
L Device:LED_RABG D6
U 1 1 5CBB1D15
P 4350 2950
F 0 "D6" H 4350 3447 50  0000 C CNN
F 1 "LED_RABG" H 4350 3356 50  0000 C CNN
F 2 "Components:LED_RGB_D5mm" H 4350 2900 50  0001 C CNN
F 3 "~" H 4350 2900 50  0001 C CNN
	1    4350 2950
	-1   0    0    -1  
$EndComp
Text Label 4550 2750 0    50   ~ 0
RED
Text Label 4550 2950 0    50   ~ 0
GREEN
Text Label 4550 3150 0    50   ~ 0
BLUE
Text Label 4150 2950 2    50   ~ 0
GND
$Comp
L Device:LED_RABG D3
U 1 1 5CBB1E1B
P 3400 2100
F 0 "D3" H 3400 2597 50  0000 C CNN
F 1 "LED_RABG" H 3400 2506 50  0000 C CNN
F 2 "Components:LED_RGB_D5mm" H 3400 2050 50  0001 C CNN
F 3 "~" H 3400 2050 50  0001 C CNN
	1    3400 2100
	-1   0    0    -1  
$EndComp
Text Label 3600 1900 0    50   ~ 0
RED
Text Label 3600 2100 0    50   ~ 0
GREEN
Text Label 3600 2300 0    50   ~ 0
BLUE
Text Label 3200 2100 2    50   ~ 0
GND
$Comp
L Device:LED_RABG D1
U 1 1 5CBB1E26
P 2450 2100
F 0 "D1" H 2450 2597 50  0000 C CNN
F 1 "LED_RABG" H 2450 2506 50  0000 C CNN
F 2 "Components:LED_RGB_D5mm" H 2450 2050 50  0001 C CNN
F 3 "~" H 2450 2050 50  0001 C CNN
	1    2450 2100
	-1   0    0    -1  
$EndComp
Text Label 2650 1900 0    50   ~ 0
RED
Text Label 2650 2100 0    50   ~ 0
GREEN
Text Label 2650 2300 0    50   ~ 0
BLUE
Text Label 2250 2100 2    50   ~ 0
GND
$Comp
L Device:LED_RABG D4
U 1 1 5CBB1E31
P 3400 2950
F 0 "D4" H 3400 3447 50  0000 C CNN
F 1 "LED_RABG" H 3400 3356 50  0000 C CNN
F 2 "Components:LED_RGB_D5mm" H 3400 2900 50  0001 C CNN
F 3 "~" H 3400 2900 50  0001 C CNN
	1    3400 2950
	-1   0    0    -1  
$EndComp
Text Label 3600 2750 0    50   ~ 0
RED
Text Label 3600 2950 0    50   ~ 0
GREEN
Text Label 3600 3150 0    50   ~ 0
BLUE
Text Label 3200 2950 2    50   ~ 0
GND
$Comp
L Device:LED_RABG D2
U 1 1 5CBB1E3C
P 2450 2950
F 0 "D2" H 2450 3447 50  0000 C CNN
F 1 "LED_RABG" H 2450 3356 50  0000 C CNN
F 2 "Components:LED_RGB_D5mm" H 2450 2900 50  0001 C CNN
F 3 "~" H 2450 2900 50  0001 C CNN
	1    2450 2950
	-1   0    0    -1  
$EndComp
Text Label 2650 2750 0    50   ~ 0
RED
Text Label 2650 2950 0    50   ~ 0
GREEN
Text Label 2650 3150 0    50   ~ 0
BLUE
Text Label 2250 2950 2    50   ~ 0
GND
$Comp
L Device:R R1
U 1 1 5CBB230D
P 5650 2500
F 0 "R1" V 5550 2500 50  0000 C CNN
F 1 "120" V 5650 2500 50  0000 C CNN
F 2 "Components:R_W7.62mmD0.8mm" V 5580 2500 50  0001 C CNN
F 3 "~" H 5650 2500 50  0001 C CNN
	1    5650 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5CBB23BA
P 5650 2700
F 0 "R2" V 5550 2700 50  0000 C CNN
F 1 "120" V 5650 2700 50  0000 C CNN
F 2 "Components:R_W7.62mmD0.8mm" V 5580 2700 50  0001 C CNN
F 3 "~" H 5650 2700 50  0001 C CNN
	1    5650 2700
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5CBB23E4
P 5650 2800
F 0 "R3" V 5750 2800 50  0000 C CNN
F 1 "120" V 5650 2800 50  0000 C CNN
F 2 "Components:R_W7.62mmD0.8mm" V 5580 2800 50  0001 C CNN
F 3 "~" H 5650 2800 50  0001 C CNN
	1    5650 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 2500 6000 2500
Wire Wire Line
	5800 2700 6000 2700
Wire Wire Line
	5800 2800 6000 2800
$Comp
L Mechanical:MountingHole H1
U 1 1 5CBB2787
P 5150 1850
F 0 "H1" H 5250 1896 50  0000 L CNN
F 1 "MountingHole" H 5250 1805 50  0000 L CNN
F 2 "Components:Mounting_Hole_3.2mm" H 5150 1850 50  0001 C CNN
F 3 "~" H 5150 1850 50  0001 C CNN
	1    5150 1850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5CBB2850
P 5150 2100
F 0 "H2" H 5250 2146 50  0000 L CNN
F 1 "MountingHole" H 5250 2055 50  0000 L CNN
F 2 "Components:Mounting_Hole_3.2mm" H 5150 2100 50  0001 C CNN
F 3 "~" H 5150 2100 50  0001 C CNN
	1    5150 2100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
