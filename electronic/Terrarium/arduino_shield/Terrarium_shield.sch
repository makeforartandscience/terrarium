EESchema Schematic File Version 4
LIBS:Terrarium_shield-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "terrarium Project / arduino uno shield"
Date "2019-04-15"
Rev ""
Comp "Thierry Dassé 2019"
Comment1 "CC-BY-NC-SA"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:Arduino_UNO_R3 A1
U 1 1 5C94C18D
P 2800 2350
F 0 "A1" H 2800 3750 50  0000 C CNN
F 1 "Arduino_UNO_R3" H 2800 3650 50  0000 C CNN
F 2 "Components:Arduino_Shield" H 2950 1300 50  0001 L CNN
F 3 "https://www.arduino.cc/en/Main/arduinoBoardUno" H 2600 3400 50  0001 C CNN
	1    2800 2350
	1    0    0    -1  
$EndComp
$Comp
L Sensor:DHT11 U1
U 1 1 5C94C25D
P 4950 1850
F 0 "U1" V 4583 1850 50  0000 C CNN
F 1 "DHT11" V 4674 1850 50  0000 C CNN
F 2 "Components:WR-WTB_04" H 5100 2100 50  0001 C CNN
F 3 "http://akizukidenshi.com/download/ds/aosong/DHT11.pdf" H 5100 2100 50  0001 C CNN
	1    4950 1850
	0    1    1    0   
$EndComp
$Comp
L Driver_Motor:L293D U2
U 1 1 5C94C31B
P 6750 2500
F 0 "U2" H 6750 3950 50  0000 C CNN
F 1 "L293D" H 6750 3850 50  0000 C CNN
F 2 "Components:DIP16_W7.16mmD0.8mm" H 7000 1750 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/l293.pdf" H 6450 3200 50  0001 C CNN
	1    6750 2500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5C94C410
P 4400 3100
F 0 "J3" H 4480 3092 50  0000 L CNN
F 1 "Conn_01x04_RGBLED1" H 4480 3001 50  0000 L CNN
F 2 "Components:WR-WTB_04" H 4400 3100 50  0001 C CNN
F 3 "~" H 4400 3100 50  0001 C CNN
	1    4400 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 5C94C56A
P 4400 4100
F 0 "J5" H 4479 4142 50  0000 L CNN
F 1 "Conn_01x03_SOIL_MOISTURE" H 4479 4051 50  0000 L CNN
F 2 "Components:WR-WTB_03" H 4400 4100 50  0001 C CNN
F 3 "~" H 4400 4100 50  0001 C CNN
	1    4400 4100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 5C94C6C5
P 4400 4500
F 0 "J6" H 4479 4492 50  0000 L CNN
F 1 "Conn_01x02_WATER_PUMP" H 4479 4401 50  0000 L CNN
F 2 "Components:WR-WTB_02" H 4400 4500 50  0001 C CNN
F 3 "~" H 4400 4500 50  0001 C CNN
	1    4400 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 5C94CB08
P 2650 4000
F 0 "J7" H 2730 4042 50  0000 L CNN
F 1 "Conn_01x03_FAN" H 2730 3951 50  0000 L CNN
F 2 "Components:WR-WTB_03" H 2650 4000 50  0001 C CNN
F 3 "~" H 2650 4000 50  0001 C CNN
	1    2650 4000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J8
U 1 1 5C94CE20
P 2650 4500
F 0 "J8" H 2730 4542 50  0000 L CNN
F 1 "Conn_01x03_PH_SENSOR" H 2730 4451 50  0000 L CNN
F 2 "Components:WR-WTB_03" H 2650 4500 50  0001 C CNN
F 3 "~" H 2650 4500 50  0001 C CNN
	1    2650 4500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5C94D161
P 4400 2600
F 0 "J2" H 4480 2592 50  0000 L CNN
F 1 "Conn_01x02_LIGHT_SENSOR" H 4480 2501 50  0000 L CNN
F 2 "Components:WR-WTB_02" H 4400 2600 50  0001 C CNN
F 3 "~" H 4400 2600 50  0001 C CNN
	1    4400 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Barrel_Jack_Switch J1
U 1 1 5C94E067
P 3950 1200
F 0 "J1" H 4005 1517 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 4005 1426 50  0000 C CNN
F 2 "Components:Conn_DC-10" H 4000 1160 50  0001 C CNN
F 3 "~" H 4000 1160 50  0001 C CNN
	1    3950 1200
	1    0    0    -1  
$EndComp
Text Label 4250 1100 0    50   ~ 0
12V
$Comp
L power:+12V #PWR01
U 1 1 5C94E2B7
P 4950 950
F 0 "#PWR01" H 4950 800 50  0001 C CNN
F 1 "+12V" H 4965 1123 50  0000 C CNN
F 2 "" H 4950 950 50  0001 C CNN
F 3 "" H 4950 950 50  0001 C CNN
	1    4950 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1100 4950 1100
Wire Wire Line
	4950 1100 4950 950 
Text Label 2700 1350 1    50   ~ 0
12V
Text Label 6850 1500 1    50   ~ 0
12V
Text Label 4250 1200 0    50   ~ 0
GND
Text Label 4250 1300 0    50   ~ 0
GND
Text Label 2700 3450 3    50   ~ 0
GND
Text Label 2800 3450 3    50   ~ 0
GND
Text Label 2900 3450 3    50   ~ 0
GND
Text Label 4650 1750 2    50   ~ 0
GND
Text Label 6550 3300 3    50   ~ 0
GND
Text Label 6650 3300 3    50   ~ 0
GND
Text Label 6850 3300 3    50   ~ 0
GND
Text Label 6950 3300 3    50   ~ 0
GND
Text Label 6650 1500 1    50   ~ 0
5V
Text Label 5250 1750 0    50   ~ 0
5V
Text Label 3000 1350 1    50   ~ 0
5V
Text Label 4200 4000 2    50   ~ 0
GND
Text Label 4200 4100 2    50   ~ 0
5V
Text Label 4200 4200 2    50   ~ 0
SOIL
Text Label 3300 2350 0    50   ~ 0
SOIL
Text Label 4200 3000 2    50   ~ 0
RED
Text Label 4200 3100 2    50   ~ 0
GND
Text Label 4200 3200 2    50   ~ 0
BLUE
Text Label 4200 3300 2    50   ~ 0
GREEN
Text Label 2300 2750 2    50   ~ 0
BLUE
Text Label 2450 3900 2    50   ~ 0
GND
Text Label 2450 4000 2    50   ~ 0
FAN_OUT
NoConn ~ 2450 4100
Text Label 4200 4500 2    50   ~ 0
GND
Text Label 4200 4600 2    50   ~ 0
PUMP_OUT
Text Label 4950 2150 3    50   ~ 0
DHT
Text Label 2300 2150 2    50   ~ 0
DHT
NoConn ~ 2300 1750
NoConn ~ 2300 1850
NoConn ~ 3300 1750
NoConn ~ 3300 1950
NoConn ~ 3300 2150
Text Label 2450 4400 2    50   ~ 0
GND
Text Label 2450 4500 2    50   ~ 0
5V
Text Label 2450 4600 2    50   ~ 0
PH
Text Label 3300 2450 0    50   ~ 0
PH
Text Label 7250 1900 0    50   ~ 0
FAN_OUT
Text Label 2300 1950 2    50   ~ 0
FAN_COM
Text Label 6250 1900 2    50   ~ 0
FAN_COM
Text Label 6250 2300 2    50   ~ 0
5V
Text Label 2300 2050 2    50   ~ 0
PUMP_COM
Text Label 6250 2100 2    50   ~ 0
PUMP_COM
Text Label 7250 2100 0    50   ~ 0
PUMP_OUT
NoConn ~ 6250 2500
NoConn ~ 6250 2700
NoConn ~ 6250 2900
NoConn ~ 7250 2500
NoConn ~ 7250 2700
Text Label 4200 2600 2    50   ~ 0
GND
Text Label 4200 2700 2    50   ~ 0
LIGHT
Text Label 3300 2550 0    50   ~ 0
LIGHT
$Comp
L Device:R R1
U 1 1 5C950134
P 3800 2700
F 0 "R1" V 3700 2700 50  0000 C CNN
F 1 "10k" V 3800 2700 50  0000 C CNN
F 2 "Components:R_W7.62mmD0.8mm" V 3730 2700 50  0001 C CNN
F 3 "~" H 3800 2700 50  0001 C CNN
	1    3800 2700
	0    1    1    0   
$EndComp
Text Label 3650 2700 2    50   ~ 0
5V
Wire Wire Line
	3950 2700 4200 2700
NoConn ~ 2300 2250
NoConn ~ 2300 2350
NoConn ~ 2300 2450
NoConn ~ 2300 2550
NoConn ~ 2300 2950
NoConn ~ 2300 3050
NoConn ~ 3300 3150
NoConn ~ 3300 3050
NoConn ~ 3300 2850
NoConn ~ 3300 2750
NoConn ~ 3300 2650
NoConn ~ 2900 1350
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5C950BB5
P 4950 1100
F 0 "#FLG0101" H 4950 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 4950 1273 50  0000 C CNN
F 2 "" H 4950 1100 50  0001 C CNN
F 3 "~" H 4950 1100 50  0001 C CNN
	1    4950 1100
	-1   0    0    1   
$EndComp
Connection ~ 4950 1100
$Comp
L power:GND #PWR0101
U 1 1 5C950C4D
P 5500 1100
F 0 "#PWR0101" H 5500 850 50  0001 C CNN
F 1 "GND" H 5505 927 50  0000 C CNN
F 2 "" H 5500 1100 50  0001 C CNN
F 3 "" H 5500 1100 50  0001 C CNN
	1    5500 1100
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5C950CEE
P 5500 1100
F 0 "#FLG0102" H 5500 1175 50  0001 C CNN
F 1 "PWR_FLAG" H 5500 1274 50  0000 C CNN
F 2 "" H 5500 1100 50  0001 C CNN
F 3 "~" H 5500 1100 50  0001 C CNN
	1    5500 1100
	1    0    0    -1  
$EndComp
Text Label 2300 2650 2    50   ~ 0
GREEN
Text Label 2300 2850 2    50   ~ 0
RED
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 5CB2E145
P 4400 3600
F 0 "J4" H 4479 3592 50  0000 L CNN
F 1 "Conn_01x04_RGBLED2" H 4479 3501 50  0000 L CNN
F 2 "Components:WR-WTB_04" H 4400 3600 50  0001 C CNN
F 3 "~" H 4400 3600 50  0001 C CNN
	1    4400 3600
	1    0    0    -1  
$EndComp
Text Label 4200 3500 2    50   ~ 0
RED
Text Label 4200 3600 2    50   ~ 0
GND
Text Label 4200 3700 2    50   ~ 0
BLUE
Text Label 4200 3800 2    50   ~ 0
GREEN
$EndSCHEMATC
