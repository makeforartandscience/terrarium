#!/usr/bin/python

import drill2gcode

project = drill2gcode.Drill2Gcode(header='../../gcode_header/header.tap', footer='../../gcode_header/footer.tap')
project.load('../gerber/Terrarium_shield-NPTH.drl')
project.mirrorY(37)
project.makeGcode('../gcode/Terrarium_shield','.tap')
