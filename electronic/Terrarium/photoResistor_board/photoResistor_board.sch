EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5CC3002C
P 3650 1800
F 0 "J1" H 3730 1792 50  0000 L CNN
F 1 "Conn_01x02" H 3730 1701 50  0000 L CNN
F 2 "Components:WR-WTB-H_02" H 3650 1800 50  0001 C CNN
F 3 "~" H 3650 1800 50  0001 C CNN
	1    3650 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_PHOTO R1
U 1 1 5CC30154
P 2850 1850
F 0 "R1" H 2920 1896 50  0000 L CNN
F 1 "R_PHOTO" H 2920 1805 50  0000 L CNN
F 2 "Components:PH_W2.54mmD0.8mm" V 2900 1600 50  0001 L CNN
F 3 "~" H 2850 1800 50  0001 C CNN
	1    2850 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 1800 3300 1800
Wire Wire Line
	3300 1800 3300 1700
Wire Wire Line
	3300 1700 2850 1700
Wire Wire Line
	3450 1900 3300 1900
Wire Wire Line
	3300 1900 3300 2000
Wire Wire Line
	3300 2000 2850 2000
$Comp
L Mechanical:MountingHole H1
U 1 1 5CC303E0
P 3100 1550
F 0 "H1" H 3200 1596 50  0000 L CNN
F 1 "MountingHole" H 3200 1505 50  0000 L CNN
F 2 "Components:Mounting_Hole_3.2mm" H 3100 1550 50  0001 C CNN
F 3 "~" H 3100 1550 50  0001 C CNN
	1    3100 1550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5CC303FE
P 3100 2150
F 0 "H2" H 3200 2196 50  0000 L CNN
F 1 "MountingHole" H 3200 2105 50  0000 L CNN
F 2 "Components:Mounting_Hole_3.2mm" H 3100 2150 50  0001 C CNN
F 3 "~" H 3100 2150 50  0001 C CNN
	1    3100 2150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
